import React, {Fragment, useState} from 'react'



export const Listado = ()=> {



    const [datos, setDatos] = useState([
        {nombre:'Rodrigo', edad:20},
        {nombre:'Patri', edad:23},
        {nombre:'Rober', edad:50},
        {nombre:'Geo', edad:54}
    ]);

    
    return(
            <Fragment>
                <ul className="list-group list-group-item-success">
                {
                datos.map( (item, index) => {
                            return <li className="list-group-item " key={index} >{item.nombre}-{item.edad}</li>
                            }
                        )
                    }
            </ul>
            </Fragment>

    )

}


export default Listado