import React, { Component, useState } from 'react'



const Formulario = () => {

    const [nombre, setNombre] = useState('');
    const [edad, setEdad] = useState('');

    const setNombreEvent= (e) =>{
        const nameCapitalized = e.target.value.charAt(0).toUpperCase() + e.target.value.slice(1)
        setNombre(nameCapitalized)
    }
    
    const estaViejo= ()=> {
        if(edad > 30){

            return 'esta viejo';
        }else{
            return 'es joven';
        }
    }


    const validar = (event) => {
            event.preventDefault();
            if(!nombre.trim()){
                
            }

    }


    return (
        <div>
        <h2> soy {nombre} y tengo {edad} años de edad</h2>
        <h3>{estaViejo()}</h3>
        <form onSubmit={validar} className="form-group">
            <input placeholder="ingrese nombre" 
             className="form-control mb-3"
             type="text"
             onChange={setNombreEvent}                     
             />

            <input 
            placeholder="ingrese edad" 
            className="form-control mb-3" type="number" 
            onChange={(e)=> {setEdad(e.target.value)}}                     
            />
            <input type="submit" className="btn btn-info mb-3"   value="ingresar" />
        </form>
        
    </div>
    )
    
}

export default Formulario


