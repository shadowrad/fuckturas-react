import React, { Component, Fragment } from 'react'
import BootHeader from './BootHeader' 
import Footer from './Footer'
import Formulario from './Formulario'
import Tarjetas from './Tarjetas'


export default class Contenedor extends Component {
    
    render() {
        return (
            <Fragment>
                    <BootHeader></BootHeader>
                    <div className="container">
                    <Tarjetas></Tarjetas>
                        <div className='row'>
                            <div className="col-12">
                                <Formulario></Formulario>
                            </div>
                        </div>
                    </div>
               <Footer></Footer>
            </Fragment>
        )
    }
}



