import React,{useState} from 'react'

const Tarjeta = (props) => {
    const [titulo,setTitulo]= useState(props.obj.descripcion)
    
    //const id_tarjeta = props.id
    //const fecha_cierre =props.fecha_cierre

    return (
        <div key={"card_"+props.obj.id} className="card mb-3" style={{width: "18rem"}}>
        <div className="card-body">
            <h5 className="card-title">{props.obj.descripcion}</h5>
            <h6 className="card-subtitle mb-2 text-muted">Card</h6>
            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <a href="#" className="card-btn">Card link</a>
            <a href="#" className="card-link">Another link</a>
        </div>
    </div>
    )
}

export default Tarjeta
