import React, {Fragment, useState} from 'react'

export const Contador = ()=> {

    const [grados,setGrados] = useState(25)


    const incrementar = () => {
        setGrados(grados+1)
    }
    
    const decrementar = () => {
        setGrados(grados-1)
    }
    

    return(
            <Fragment>
                <h2>Hay {grados} grados </h2>
                <h3 className='' >
                    { grados <= 5 ? 'Frio de la re concha':''}
                    { grados > 5 && grados <= 17 ? 'Hace frio':''}
                    { grados > 17 && grados < 30 ? 'Hace calor':''}
                    { grados >= 30 ? 'Calor de puta madre':''}
                 </h3>
                <button className='btn btn-info' onClick={incrementar} >incrementar</button>
                <button className='btn btn-danger' onClick={decrementar}> decrementar </button>
            </Fragment>

    )

}


export default Contador