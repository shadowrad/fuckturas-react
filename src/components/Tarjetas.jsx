import React, {useState, Fragment} from 'react'
import Tarjeta from './Tarjeta'

const Tarjetas = () => {

   var tarjetas_temp = [
        [
            {
                "id": 1,
                "descripcion": "Tarjeta Visa1",
                "fecha_cierre": "2021-05-10",
                "user": 1
            }
        ]
    ]
    const [lista,setLista]= useState(tarjetas_temp)




    const get_group_rows = (flat_array, cols)=>{
        var arrays = []
        var row_size = cols
            
        for (let i = 0; i < flat_array.length; i += row_size)
        arrays.push(flat_array.slice(i, i + row_size));

        return arrays

    }

    const actualizar_lista_tarjetas = (array_tarjetas)=>{
        const tarjetasRows = get_group_rows(array_tarjetas,2)
        console.log(tarjetasRows)
        setLista(tarjetasRows)
    }

    const buscar_tarjetas = ()=>{
        fetch('http://localhost:8000/tarjetas/')
        .then(response => response.json())
        .then(json => actualizar_lista_tarjetas(json.results))
        .catch( e => console.log(e))
    }



    return (
        <Fragment>
        <button onClick={e => {buscar_tarjetas()}} >TRAER TARJETAS</button>
            {
                lista.map((row,idx) =>{
                    return  <div className="row" key={"row_"+idx} >
                               {
                                   row.map((item,c_idx) =>{
                                       return <div className="col-4">
                                            <Tarjeta key={"comp_tarjeta_"+item.id} 
                                                obj = {item}
                                            ></Tarjeta>
                                        </div>
                                   })
                                
                                }
                            </div>
            
                    } )
            }

        
    </Fragment>
    )
}

export default Tarjetas
