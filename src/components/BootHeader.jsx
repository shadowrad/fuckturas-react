import React, { Fragment,Component } from 'react'
import Menu from './Menu'

export default class BootHeader extends Component {
    render() {
        return (
            <Fragment>
                <div className="jumbotron text-center" style={{marginBottom:0}}>
                  <h1>Bootstrap con react!!! </h1>
                  <p>Resize this responsive page to see the effect!</p> 
                </div>
                 <Menu></Menu>
            </Fragment>
        )
    }
}